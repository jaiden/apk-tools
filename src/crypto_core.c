#include <openssl/evp.h>
#include <sodium.h>

#include "apk_crypto.h"
#include "apk_defines.h"

#if OPENSSL_VERSION_NUMBER < 0x1010000fL || (defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x2070000fL)
#define APK_NEED_LIBCRYPTO_CLEANUP 1
#endif

#ifdef APK_NEED_LIBCRYPTO_CLEANUP
static void apk_crypto_cleanup(void)
{
	EVP_cleanup();

#ifndef OPENSSL_NO_ENGINE
	ENGINE_cleanup();
#endif
	CRYPTO_cleanup_all_ex_data();
}
#endif

int apk_crypto_init(void)
{
	if (sodium_init() < 0) {
		return -APKE_CRYPTO_ERROR;
	}

#ifdef APK_NEED_LIBCRYPTO_CLEANUP
	atexit(apk_crypto_cleanup);
	OpenSSL_add_all_algorithms();

#ifndef OPENSSL_NO_ENGINE
	ENGINE_load_builtin_engines();
	ENGINE_register_all_complete();
#endif

#endif

	return 0;
}

#undef APK_NEED_LIBCRYPTO_CLEANUP
