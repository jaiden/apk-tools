#include <errno.h>
#include <fcntl.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <sodium.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "apk_crypto.h"

/* includes '\0', undef in the end */
#define APK_CUTE_PUBLIC_SIZE 69

/*
 * The format is as follows:
 *     uint8[ 2] header = {'q', 't'}
 *     uint8[16] key_id
 *     uint8[32] public_key
 */
static int public_key_load_cute(struct apk_public_key *pub, int fd)
{
	char b64[APK_CUTE_PUBLIC_SIZE];
	uint8_t raw[50];
	size_t len;

	if (read(fd, b64, APK_CUTE_PUBLIC_SIZE) != APK_CUTE_PUBLIC_SIZE) {
		return -errno;
	}

	if (sodium_base642bin(raw, 50, b64, APK_CUTE_PUBLIC_SIZE, NULL, &len, NULL, sodium_base64_VARIANT_ORIGINAL)
	    != 0) {
		return -APKE_CRYPTO_KEY_FORMAT;
	}

	if (len != 50) {
		return -APKE_CRYPTO_KEY_FORMAT;
	}

	if (raw[0] != 'q' || raw[1] != 't') {
		return -APKE_CRYPTO_KEY_FORMAT;
	}

	pub->impl = malloc(crypto_sign_ed25519_PUBLICKEYBYTES);
	if (pub->impl == NULL) {
		return -ENOMEM;
	}

	pub->version = APK_KEY_VERSION_CUTE;
	memcpy(pub->id, raw + 2, sizeof pub->id);
	memcpy(pub->impl, raw + 18, crypto_sign_ed25519_PUBLICKEYBYTES);

	return 0;
}

/*
 * The signature format is as follows:
 *     uint8[ 2] header = {'q', 't'}
 *     uint8[16] key_id
 *     uint8[64] signature
 */
static int verify_cute(struct apk_public_key *pub, struct apk_digest_ctx *dctx, void *sig, size_t len)
{
	unsigned char *sigdata = (unsigned char *) sig;
	struct apk_digest digest;

	if (len != 82) {
		return -APKE_SIGNATURE_INVALID;
	}

	if (sigdata[0] != 'q' || sigdata[1] != 't') {
		return -APKE_SIGNATURE_INVALID;
	}

	if (sodium_memcmp(pub->id, sigdata + 2, sizeof pub->id) != 0) {
		return -APKE_SIGNATURE_INVALID;
	}

	if (dctx->alg != APK_DIGEST_SHA256) {
		return -APKE_SIGNATURE_INVALID;
	}

	if (apk_digest_ctx_final(dctx, &digest) != 0) {
		return -APKE_SIGNATURE_INVALID;
	}

	if (crypto_sign_ed25519_verify_detached(sigdata + 18, digest.data, digest.len, pub->impl) != 0) {
		return -APKE_SIGNATURE_INVALID;
	}

	return 0;
}

static void public_key_free_cute(struct apk_public_key *pub)
{
	free(pub->impl);
}

static int public_key_load_v2(struct apk_public_key *pub, int fd)
{
	unsigned char *raw = NULL;
	struct apk_digest digest;
	EVP_PKEY *pkey;
	BIO *bio;
	int len;
	int r = -APKE_CRYPTO_KEY_FORMAT;

	bio = BIO_new_fd(fd, BIO_NOCLOSE);
	if (bio == NULL) {
		r = -ENOMEM;
		goto err_bio_new;
	}

	pkey = PEM_read_bio_PUBKEY(bio, NULL, NULL, NULL);
	if (pkey == NULL) {
		r = -APKE_CRYPTO_KEY_FORMAT;
		goto err_pem_load;
	}

	if (EVP_PKEY_id(pkey) != EVP_PKEY_RSA) {
		r = -APKE_CRYPTO_KEY_FORMAT;
		goto err_load_and_check_type;
	}

	len = i2d_PublicKey(pkey, &raw);
	if (len < 0) {
		r = -APKE_CRYPTO_ERROR;
		goto err_load_and_check_type;
	}

	if (apk_digest_calc(&digest, APK_DIGEST_SHA512, raw, len) != 0) {
		r = -APKE_CRYPTO_ERROR;
		goto err_calculate_id_and_finalize;
	}

	pub->impl = pkey;
	if (EVP_PKEY_up_ref(pkey) != 1) {
		r = -APKE_CRYPTO_ERROR;
		goto err_calculate_id_and_finalize;
	}

	pub->version = APK_KEY_VERSION_V2;
	memcpy(pub->id, digest.data, sizeof pub->id);

	r = 0;

err_calculate_id_and_finalize:
	OPENSSL_free(raw);
err_load_and_check_type:
	EVP_PKEY_free(pkey);
err_pem_load:
	BIO_free(bio);
err_bio_new:
	return r;
}

/*
 * RSA-PKCS1 v1.5 can work with different hashes.
 * Since APK v2 hasn't switched from SHA-1 yet, we still have to work with it for now.
 * However, MD5 is out of the question.
 */
static int verify_v2(struct apk_public_key *pub, struct apk_digest_ctx *dctx, void *sig, size_t len)
{
	struct apk_digest digest;
	EVP_PKEY_CTX *pctx;
	const EVP_MD *md;
	int r = -APKE_SIGNATURE_INVALID;

	if (apk_digest_ctx_final(dctx, &digest) != 0) {
		r = -APKE_SIGNATURE_INVALID;
		goto err_digest_final_and_pctx_new;
	}

	switch (dctx->alg) {
	case APK_DIGEST_SHA1:
		md = EVP_sha1();
		break;
	case APK_DIGEST_SHA256:
		md = EVP_sha256();
		break;
	case APK_DIGEST_SHA512:
		md = EVP_sha512();
		break;
	case APK_DIGEST_NONE:
	case APK_DIGEST_MD5:
	default:
		r = -APKE_SIGNATURE_INVALID;
		goto err_digest_final_and_pctx_new;
	}

	pctx = EVP_PKEY_CTX_new(pub->impl, NULL);
	if (pctx == NULL) {
		r = -APKE_SIGNATURE_INVALID;
		goto err_pctx_setup_and_verify;
	}

	if (EVP_PKEY_verify_init(pctx) != 1) {
		r = -APKE_SIGNATURE_INVALID;
		goto err_pctx_setup_and_verify;
	}

	if (EVP_PKEY_CTX_set_rsa_padding(pctx, RSA_PKCS1_PADDING) != 1) {
		r = -APKE_SIGNATURE_INVALID;
		goto err_pctx_setup_and_verify;
	}

	if (EVP_PKEY_CTX_set_signature_md(pctx, md) != 1) {
		r = -APKE_SIGNATURE_INVALID;
		goto err_pctx_setup_and_verify;
	}

	if (EVP_PKEY_verify(pctx, sig, len, digest.data, digest.len) != 1) {
		r = -APKE_SIGNATURE_INVALID;
		goto err_pctx_setup_and_verify;
	}

	r = 0;

err_pctx_setup_and_verify:
	EVP_PKEY_CTX_free(pctx);
err_digest_final_and_pctx_new:
	return r;
}

static void public_key_free_v2(struct apk_public_key *pub)
{
	EVP_PKEY_free(pub->impl);
}

int apk_public_key_load(struct apk_public_key *pub, int dirfd, const char *fn)
{
	struct stat st;
	int fd = -1;
	int r  = -APKE_CRYPTO_KEY_FORMAT;

	fd = openat(dirfd, fn, O_RDONLY | O_CLOEXEC);
	if (fd == -1) {
		r = -errno;
		goto err_openat;
	}

	if (fstat(fd, &st) != 0) {
		r = -errno;
		goto err_fstat;
	}

	// guess
	if (st.st_size == APK_CUTE_PUBLIC_SIZE) {
		r = public_key_load_cute(pub, fd);
	} else {
		r = public_key_load_v2(pub, fd);
	}

err_fstat:
	close(fd);
err_openat:
	return r;
}

void apk_public_key_free(struct apk_public_key *pub)
{
	if (pub == NULL) {
		return;
	}

	if (pub->impl == NULL) {
		return;
	}

	switch (pub->version) {
	case APK_KEY_VERSION_V2:
		public_key_free_v2(pub);
		break;
	case APK_KEY_VERSION_CUTE:
		public_key_free_cute(pub);
		break;
	default:
		return;
	}

	pub->impl = NULL;
}

int apk_verify_digest_start(struct apk_digest_ctx *dctx, uint16_t signature_type)
{
	uint8_t digest;

	switch (signature_type) {
	case APK_SIGNATURE_CUTE:
	case APK_SIGNATURE_RSA256:
		digest = APK_DIGEST_SHA256;
		break;
	case APK_SIGNATURE_RSA512:
		digest = APK_DIGEST_SHA512;
		break;
	case APK_SIGNATURE_RSA:
		digest = APK_DIGEST_SHA1;
		break;
	default:
		return -APKE_CRYPTO_NOT_SUPPORTED;
	}

	if (apk_digest_ctx_reset(dctx, digest) != 0) {
		return -APKE_CRYPTO_ERROR;
	}

	return 0;
}

int apk_verify(struct apk_public_key *pub, struct apk_digest_ctx *dctx, void *sig, size_t len)
{
	switch (pub->version) {
	case APK_KEY_VERSION_V2:
		return verify_v2(pub, dctx, sig, len);
	case APK_KEY_VERSION_CUTE:
		return verify_cute(pub, dctx, sig, len);
	default:
		return -APKE_CRYPTO_NOT_SUPPORTED;
	}
}

#undef APK_CUTE_PUBLIC_SIZE
